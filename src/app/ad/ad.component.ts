import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Ad from '../models/ad.model';
import User from '../models/user.model';
import { AdService } from '../services/ad.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.css'],
})
export class AdComponent implements OnInit {
  ad: Ad;
  user: User;
  isLoading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private adService: AdService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.adService.getAdById(id).subscribe((ad) => {
      this.userService.getUserById(ad.user_id).subscribe((user) => {
        this.ad = ad;
        this.user = user;
        this.isLoading = false;
      });
    });
  }

  get enginePower(): string {
    const kw = Math.round(this.ad.enginePower * 0.745699872);
    return `${this.ad.enginePower} HP / ${kw} KW`;
  }

  get price(): string {
    return this.ad.price.toLocaleString('me', {
      currency: 'EUR',
    });
  }
  get drivetrain(): string {
    switch (this.ad.powertrainType) {
      case 'REAR_WHEEL_DRIVE':
        return 'Rear wheel drive';
      case 'ALL_WHEEL_DRIVE':
        return 'All wheel drive';
      case 'FRONT_WHEEL_DRIVE':
        return 'Front wheel drive';
      default:
        return 'No info';
    }
  }

  get showcaseImage(): string {
    return this.ad.showcaseImage;
  }
  get images(): string[] {
    return this.ad.images;
  }

  get publishedAt(): string {
    return new Date(this.ad.created_at).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }
}
