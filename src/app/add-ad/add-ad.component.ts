import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AdService } from '../services/ad.service';
import { UserService } from '../services/user.service';
import { BIKES, BIKE_BODIES } from '../values/bikes.value';
import { CARS, CAR_BODIES } from '../values/cars.value';
import { LOCATIONS } from '../values/locations.value';
import { TRUCKS, TRUCK_BODIES } from '../values/trucks.value';

@Component({
  selector: 'app-add-ad',
  templateUrl: './add-ad.component.html',
  styleUrls: ['./add-ad.component.css'],
})
export class AddAdComponent implements OnInit {
  private images: FileList;
  private showcaseImage: FileList;

  isPublished: boolean = false;

  cars: string[] = CARS;
  carBodies: string[] = CAR_BODIES;
  bikes: string[] = BIKES;
  bikeBodies: string[] = BIKE_BODIES;
  trucks: string[] = TRUCKS;
  truckBodies: string[] = TRUCK_BODIES;
  locations: string[] = LOCATIONS;

  addForm = new FormGroup({
    category: new FormControl('', Validators.required),
    make: new FormControl('', Validators.required),
    body: new FormControl('', Validators.required),
    model: new FormControl('', Validators.required),
    engineCC: new FormControl('', Validators.pattern('\\d+')),
    hp: new FormControl('', Validators.pattern('\\d+')),
    wheelDrive: new FormControl('', Validators.required),
    fuel: new FormControl('', Validators.required),
    transmission: new FormControl('', Validators.required),
    gears: new FormControl('', Validators.pattern('\\d+')),
    damage: new FormControl('', Validators.required),
    modelYear: new FormControl('', Validators.pattern('\\d{4}')),
    distance: new FormControl('', Validators.pattern('\\d+')),
    showcaseImg: new FormControl('', Validators.required),
    images: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    price: new FormControl('', Validators.pattern('\\d+')),
    desc: new FormControl('', Validators.required),
    addr: new FormControl('', Validators.required),
  });

  constructor(
    private adService: AdService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSubmit(event: Event) {
    event.preventDefault();

    const { showcaseImg, images, ...other } = this.addForm.value;
    const data = { ...other, images: [], showcaseImage: null };

    Array.from(this.images).forEach((image) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(image);
      fileReader.onload = () => {
        data.images.push(fileReader.result.toString());
      };
    });

    const showcaseImage = this.showcaseImage.item(0);
    const fileReader = new FileReader();
    fileReader.readAsDataURL(showcaseImage);
    fileReader.onload = () => {
      data.showcaseImage = fileReader.result.toString();
    };

    console.dir(data);

    this.userService.getCurrentUserDetails().subscribe((user) => {
      this.adService.createAd(data, user.id).subscribe((res) => {
        this.isPublished = true;
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 1000);
      });
    });
  }

  onImagesChange(files: FileList) {
    this.images = files;
  }

  onShowcaseImageChange(file: FileList) {
    this.showcaseImage = file;
  }

  get engineCC() {
    return this.addForm.get('engineCC');
  }

  get hp() {
    return this.addForm.get('hp');
  }

  get gears() {
    return this.addForm.get('gears');
  }

  get modelYear() {
    return this.addForm.get('modelYear');
  }

  get distance() {
    return this.addForm.get('distance');
  }

  get price() {
    return this.addForm.get('price');
  }
}
