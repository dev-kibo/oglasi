import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdComponent } from './ad/ad.component';
import { AddAdComponent } from './add-ad/add-ad.component';
import { DashAdsComponent } from './dashboard/dash-ads/dash-ads.component';
import { DashStatsComponent } from './dashboard/dash-stats/dash-stats.component';
import { DashUsersComponent } from './dashboard/dash-users/dash-users.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditAdComponent } from './edit-ad/edit-ad.component';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileAdsComponent } from './profile/profile-ads/profile-ads.component';
import { ProfilePageComponent } from './profile/profile-page/profile-page.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'edit/ad/:id',
    component: EditAdComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'register',
    component: RegisterComponent,
    pathMatch: 'full',
  },
  {
    path: 'ad/:id',
    component: AdComponent,
    pathMatch: 'full',
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ProfilePageComponent,
      },
      {
        path: 'my-ads',
        component: ProfileAdsComponent,
      },
    ],
  },
  {
    path: 'publish/ad',
    component: AddAdComponent,
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    canActivate: [RoleGuard],
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: DashStatsComponent,
      },
      {
        path: 'users',
        component: DashUsersComponent,
      },
      {
        path: 'ads',
        component: DashAdsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
