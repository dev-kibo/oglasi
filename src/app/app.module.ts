import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CardComponent } from './card/card.component';
import { FiltersComponent } from './filters/filters.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AdComponent } from './ad/ad.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileNavigationComponent } from './profile/profile-navigation/profile-navigation.component';
import { ProfilePageComponent } from './profile/profile-page/profile-page.component';
import { ProfileAdsComponent } from './profile/profile-ads/profile-ads.component';
import { AddAdComponent } from './add-ad/add-ad.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SidePanelComponent } from './side-panel/side-panel.component';
import { CarsComponent } from './side-panel/cars/cars.component';
import { BikesComponent } from './side-panel/bikes/bikes.component';
import { TrucksComponent } from './side-panel/trucks/trucks.component';
import { EditModalComponent } from './profile/profile-page/edit-modal/edit-modal.component';
import { DashStatsComponent } from './dashboard/dash-stats/dash-stats.component';
import { DashUsersComponent } from './dashboard/dash-users/dash-users.component';
import { DashAdsComponent } from './dashboard/dash-ads/dash-ads.component';
import { DashNavComponent } from './dashboard/dash-nav/dash-nav.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { EditAdComponent } from './edit-ad/edit-ad.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CardComponent,
    FiltersComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AdComponent,
    ProfileComponent,
    ProfileNavigationComponent,
    ProfilePageComponent,
    ProfileAdsComponent,
    AddAdComponent,
    DashboardComponent,
    SidePanelComponent,
    CarsComponent,
    BikesComponent,
    TrucksComponent,
    EditModalComponent,
    DashStatsComponent,
    DashUsersComponent,
    DashAdsComponent,
    DashNavComponent,
    EditAdComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule,
    NgbModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
