import { Component, Input, OnInit } from '@angular/core';
import Ad from '../models/ad.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() ad: Ad;

  constructor() {}

  ngOnInit(): void {}

  get price(): string {
    return this.ad.price.toLocaleString('me', {
      currency: 'EUR',
    });
  }

  get dateOfPublish(): string {
    return new Date(this.ad.created_at).toLocaleDateString('sr', {
      month: 'long',
      day: 'numeric',
      year: 'numeric',
    });
  }
}
