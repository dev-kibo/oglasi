import { Component, OnInit } from '@angular/core';
import Ad from 'src/app/models/ad.model';
import Page from 'src/app/models/page.model';
import { AdService } from 'src/app/services/ad.service';
import { StatsService } from 'src/app/services/stats.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dash-ads',
  templateUrl: './dash-ads.component.html',
  styleUrls: ['./dash-ads.component.css'],
})
export class DashAdsComponent implements OnInit {
  result: Page<Ad>;
  results: any[] = [];
  isLoading = true;
  page = 1;

  constructor(
    private statsService: StatsService,
    private userService: UserService,
    private adService: AdService
  ) {}

  ngOnInit(): void {
    this.fetchAds();
  }

  private fetchAds(page = 0) {
    this.statsService.getAdStats(page).subscribe((page) => {
      this.result = page;
      this.results = [];
      page.data.forEach((ad) => {
        this.userService.getUserById(ad.user_id).subscribe(
          (user) => {
            this.results.push({
              ...ad,
              pub: `${user.first_name} ${user.last_name}`,
              pub_email: user.email,
            });
            this.isLoading = false;
          },
          (error) => {
            this.results.push({
              ...ad,
              pub: 'Not found',
              pub_email: 'Not found',
            });
            this.isLoading = false;
          }
        );
      });
    });
  }

  get totalAds(): number {
    return this.result.results;
  }

  get pageSize(): number {
    return this.result.pageSize;
  }

  get ads(): Ad[] {
    return this.result.data;
  }

  goTo(aPage) {
    this.fetchAds(aPage);
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  handleDelete(adId: string): void {
    this.adService.deleteAdById(adId).subscribe((result) => {
      window.location.reload();
    });
  }
}
