import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dash-nav',
  templateUrl: './dash-nav.component.html',
  styleUrls: ['./dash-nav.component.css'],
})
export class DashNavComponent implements AfterViewInit {
  constructor(private router: Router) {}

  ngAfterViewInit(): void {
    const homeTab = document.querySelector('#v-pills-home-tab');
    const profileTab = document.querySelector('#v-pills-profile-tab');
    const adsTab = document.querySelector('#v-pills-messages-tab');

    const split = this.router.url.split('/');
    const currentRoute = split[split.length - 1];

    switch (currentRoute) {
      case 'dashboard':
        homeTab.classList.add('active');
        profileTab.classList.remove('active');
        adsTab.classList.remove('active');
        break;
      case 'users':
        homeTab.classList.remove('active');
        profileTab.classList.add('active');
        adsTab.classList.remove('active');
        break;
      case 'ads':
        homeTab.classList.remove('active');
        profileTab.classList.remove('active');
        adsTab.classList.add('active');
        break;
      default:
        break;
    }
  }
}
