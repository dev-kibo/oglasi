import { Component, OnInit } from '@angular/core';
import Dashboard from 'src/app/models/dashboard.model';
import { StatsService } from 'src/app/services/stats.service';

@Component({
  selector: 'app-dash-stats',
  templateUrl: './dash-stats.component.html',
  styleUrls: ['./dash-stats.component.css'],
})
export class DashStatsComponent implements OnInit {
  stats: Dashboard;
  isLoading = true;

  constructor(private statsService: StatsService) {}

  ngOnInit(): void {
    this.statsService.getDashboardStats().subscribe(
      (data) => {
        this.stats = data;
        this.isLoading = false;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
