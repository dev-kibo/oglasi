import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import Page from 'src/app/models/page.model';
import User from 'src/app/models/user.model';
import { AdService } from 'src/app/services/ad.service';
import { StatsService } from 'src/app/services/stats.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dash-users',
  templateUrl: './dash-users.component.html',
  styleUrls: ['./dash-users.component.css'],
})
export class DashUsersComponent implements OnInit {
  results = [];
  result: Page<User>;
  isLoading = true;
  page = 1;

  constructor(
    private userService: UserService,
    private statsService: StatsService,
    private adService: AdService
  ) {}

  ngOnInit(): void {
    this.fetchUsers();
  }

  private fetchUsers(aPage = 0) {
    this.statsService.getUserStats(aPage).subscribe((stats) => {
      this.results = [];
      this.result = stats;
      stats.data.forEach((user) => {
        this.adService.getUsersAds(user.id).subscribe(
          (ads) => {
            const count = ads.data.length;
            this.results.push({ ...user, count });
            this.isLoading = false;
          },
          (error) => {
            this.results.push({ ...user, count: 0 });
            this.isLoading = false;
          }
        );
      });
    });
  }

  get totalUsers(): number {
    return this.result.results;
  }

  get users() {
    return this.results;
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  get pageSize(): number {
    return this.result.pageSize;
  }

  goTo(aPage) {
    this.fetchUsers(aPage);
  }

  handleDelete(userId: string): void {
    this.userService.deleteUserById(userId).subscribe((result) => {
      window.location.reload();
    });
  }
}
