import { Component, OnInit } from '@angular/core';
import Dashboard from '../models/dashboard.model';
import { StatsService } from '../services/stats.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  constructor(private statsService: StatsService) {}

  ngOnInit(): void {}
}
