import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Ad from '../models/ad.model';
import { AdService } from '../services/ad.service';
import { UserService } from '../services/user.service';
import { BIKES, BIKE_BODIES } from '../values/bikes.value';
import { CARS, CAR_BODIES } from '../values/cars.value';
import { LOCATIONS } from '../values/locations.value';
import { TRUCKS, TRUCK_BODIES } from '../values/trucks.value';

@Component({
  selector: 'app-edit-ad',
  templateUrl: './edit-ad.component.html',
  styleUrls: ['./edit-ad.component.css'],
})
export class EditAdComponent implements OnInit {
  isUpdated: boolean = false;
  private ad: Ad;

  isLoading = true;

  cars: string[] = CARS;
  carBodies: string[] = CAR_BODIES;
  bikes: string[] = BIKES;
  bikeBodies: string[] = BIKE_BODIES;
  trucks: string[] = TRUCKS;
  truckBodies: string[] = TRUCK_BODIES;
  locations: string[] = LOCATIONS;

  addForm = new FormGroup({});
  // addForm = new FormGroup({
  //   category: new FormControl(this.ad.vehicleCategory, Validators.required),
  //   make: new FormControl('', Validators.required),
  //   body: new FormControl('', Validators.required),
  //   model: new FormControl('', Validators.required),
  //   engineCC: new FormControl('', Validators.pattern('\\d+')),
  //   hp: new FormControl('', Validators.pattern('\\d+')),
  //   wheelDrive: new FormControl('', Validators.required),
  //   fuel: new FormControl('', Validators.required),
  //   transmission: new FormControl('', Validators.required),
  //   gears: new FormControl('', Validators.pattern('\\d+')),
  //   damage: new FormControl('', Validators.required),
  //   modelYear: new FormControl('', Validators.pattern('\\d{4}')),
  //   distance: new FormControl('', Validators.pattern('\\d+')),
  //   showcaseImg: new FormControl('', Validators.required),
  //   images: new FormControl('', Validators.required),
  //   location: new FormControl('', Validators.required),
  //   price: new FormControl('', Validators.pattern('\\d+')),
  //   desc: new FormControl('', Validators.required),
  //   addr: new FormControl('', Validators.required),
  // });

  constructor(
    private adService: AdService,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const adId = this.activatedRoute.snapshot.paramMap.get('id');
    this.adService.getAdById(adId).subscribe((ad) => {
      this.ad = ad;
      this.loadForm();
      this.isLoading = false;
    });
  }

  private loadForm() {
    this.addForm = new FormGroup({
      category: new FormControl(this.ad.vehicleCategory, Validators.required),
      make: new FormControl(this.ad.make, Validators.required),
      body: new FormControl(this.ad.vehicleType, Validators.required),
      model: new FormControl(this.ad.model, Validators.required),
      engineCC: new FormControl(this.ad.engine_capacity, [
        Validators.pattern('\\d+'),
        Validators.required,
      ]),
      hp: new FormControl(this.ad.enginePower, [
        Validators.pattern('\\d+'),
        Validators.required,
      ]),
      wheelDrive: new FormControl(this.ad.powertrainType, Validators.required),
      fuel: new FormControl(this.ad.fuelType, Validators.required),
      transmission: new FormControl(this.ad.gearType, Validators.required),
      gears: new FormControl(this.ad.number_of_gears, [
        Validators.pattern('\\d+'),
        Validators.required,
      ]),
      damage: new FormControl(this.ad.is_damaged, Validators.required),
      modelYear: new FormControl(this.ad.year_of_made, [
        Validators.pattern('\\d{4}'),
        Validators.required,
      ]),
      distance: new FormControl(this.ad.travelled, [
        Validators.pattern('\\d+'),
        Validators.required,
      ]),
      location: new FormControl(this.ad.location.city, Validators.required),
      price: new FormControl(this.ad.price, [
        Validators.pattern('\\d+'),
        Validators.required,
      ]),
      desc: new FormControl(this.ad.description, Validators.required),
      addr: new FormControl(this.ad.location.street, Validators.required),
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();

    this.adService.updateAd(this.ad.id, this.addForm.value).subscribe((res) => {
      this.isUpdated = true;
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
    });
  }

  get engineCC() {
    return this.addForm.get('engineCC');
  }

  get hp() {
    return this.addForm.get('hp');
  }

  get gears() {
    return this.addForm.get('gears');
  }

  get modelYear() {
    return this.addForm.get('modelYear');
  }

  get distance() {
    return this.addForm.get('distance');
  }

  get price() {
    return this.addForm.get('price');
  }
}
