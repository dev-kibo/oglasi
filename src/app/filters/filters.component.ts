import {
  ApplicationRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { $ } from 'jquery';
import Ad from '../models/ad.model';
import { AdService } from '../services/ad.service';
import { BIKES, BIKE_BODIES } from '../values/bikes.value';
import { CARS, CAR_BODIES } from '../values/cars.value';
import { TRUCKS, TRUCK_BODIES } from '../values/trucks.value';
import { searchFormValidator } from './searchFormValidator.validator';
// jquery
declare var $: any;

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
})
export class FiltersComponent implements OnInit {
  isShown: boolean = false;
  @Output() results = new EventEmitter<Ad[]>();
  @Output() setIsLoading = new EventEmitter();

  carMakers: string[] = CARS;
  carBodies: string[] = CAR_BODIES;

  bikeMakers: string[] = BIKES;
  bikeBodies: string[] = BIKE_BODIES;

  truckMakers: string[] = TRUCKS;
  truckBodies: string[] = TRUCK_BODIES;

  searchForm = new FormGroup(
    {
      category: new FormControl(''),
      maker: new FormControl(''),
      body: new FormControl(''),
      transmission: new FormControl(''),
      city: new FormControl(''),
      fuel: new FormControl(''),
      modelYearFrom: new FormControl('', Validators.pattern('\\d{4}')),
      modelYearTo: new FormControl('', Validators.pattern('\\d{4}')),
      distanceFrom: new FormControl('', Validators.pattern('\\d+')),
      distanceTo: new FormControl('', Validators.pattern('\\d+')),
      priceFrom: new FormControl('', Validators.pattern('\\d+')),
      priceTo: new FormControl('', Validators.pattern('\\d+')),
    },
    { validators: searchFormValidator }
  );

  constructor(
    private applicationRef: ApplicationRef,
    private adService: AdService
  ) {}

  ngOnInit(): void {
    // debug
    // $('#cp').collapse('show');

    $('#cp').on('show.bs.collapse', () => {
      this.isShown = true;
      this.applicationRef.tick();
    });
    $('#cp').on('hide.bs.collapse', () => {
      this.isShown = false;
      this.applicationRef.tick();
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();

    this.setIsLoading.emit(true);
    this.adService.getFilteredAds(this.searchForm).subscribe((res) => {
      this.results.emit(res);
    });
  }

  get modelYearFrom() {
    return this.searchForm.get('modelYearFrom');
  }

  get modelYearTo() {
    return this.searchForm.get('modelYearTo');
  }
}
