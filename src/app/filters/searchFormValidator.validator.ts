import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const searchFormValidator: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const values = control.value;

  for (const key in values) {
    if (key === 'category' && values[key].length === 0) {
      return { isInvalid: true };
    }
  }

  return null;
};
