import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AdService } from '../services/ad.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  private page: number = 1;

  isInfiniteScrollDisabled: boolean = false;
  isResponseEmpty: boolean = false;
  isLoading = true;
  ads = [];
  adsLabel = 'New Vehicle Ads';

  constructor(private adService: AdService) {}

  ngOnInit(): void {
    this.adService.getAllCars().subscribe((res) => {
      this.ads = res.data;
      this.isLoading = false;
    });
  }

  handleVehicleType(value) {
    this.isLoading = true;
    this.isInfiniteScrollDisabled = true;
    this.adsLabel = `Showing ${value.toUpperCase()}S`;

    this.adService.getCarsByType(value).subscribe(
      (res) => {
        this.isResponseEmpty = false;
        this.ads = res;
        this.isLoading = false;
      },
      (error) => {
        if (error.status === 404) {
          this.isResponseEmpty = true;
        }
      }
    );
  }

  onScroll() {
    this.adService.getAllCars(this.page).subscribe((res) => {
      this.ads = this.ads.concat(res.data);
      this.page++;
    });
  }

  handleLoading(isLoading) {
    this.isLoading = isLoading;
  }

  handleFilterResults(results) {
    this.adsLabel = 'Search results';
    this.isInfiniteScrollDisabled = true;
    this.ads = results;
    this.isLoading = false;
  }
}
