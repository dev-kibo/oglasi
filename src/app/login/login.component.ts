import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('\\S+'),
      Validators.minLength(8),
    ]),
  });

  isLoginFailed: boolean = false;

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit(event) {
    event.preventDefault();

    this.authService.login(this.loginForm.value).subscribe(
      () => {
        if (this.authService.isLoggedIn()) {
          this.router.navigate([this.authService.redirectUrl]);
        }
      },
      (error) => {
        this.isLoginFailed = true;
      }
    );
  }
}
