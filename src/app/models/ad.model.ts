export default interface Ad {
  id: string;
  user_id: string;
  location: Location;
  vehicleCategory: string;
  vehicleType: string;
  make: string;
  model: string;
  travelled: number;
  year_of_made: number;
  fuelType: string;
  price: number;
  engine_capacity: number;
  enginePower: number;
  gearType: string;
  number_of_gears: number;
  is_damaged: boolean;
  powertrainType: string;
  description: string;
  created_at: string;
  images: string[];
  showcaseImage: string;
}

interface Location {
  city: string;
  street: string;
}
