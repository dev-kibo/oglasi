export default interface Dashboard {
  users: number;
  ads: number;
  carAds: number;
  bikeAds: number;
  truckAds: number;
}
