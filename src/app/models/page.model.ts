export default interface Page<T> {
  page: number;
  results: number;
  pageSize: number;
  totalPages: number;
  data: T[];
}
