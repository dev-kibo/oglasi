import { Component, OnInit } from '@angular/core';
import Ad from 'src/app/models/ad.model';
import Page from 'src/app/models/page.model';
import { AdService } from 'src/app/services/ad.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-ads',
  templateUrl: './profile-ads.component.html',
  styleUrls: ['./profile-ads.component.css'],
})
export class ProfileAdsComponent implements OnInit {
  result: Page<Ad>;
  page: number = 1;
  isResultEmpty: boolean = false;
  isLoading: boolean = true;

  constructor(private adService: AdService, private userService: UserService) {}

  ngOnInit(): void {
    this.fetchUserAds();
  }

  private fetchUserAds(page = 0, perPage = 5) {
    this.userService.getCurrentUserDetails().subscribe(
      (user) => {
        this.adService.getUsersAds(user.id, page, perPage).subscribe(
          (page) => {
            this.result = page;
            this.isLoading = false;
          },
          (error) => {
            this.isResultEmpty = true;
            this.isLoading = false;
          }
        );
      },
      (error) => {}
    );
  }

  get ads(): Ad[] {
    return this.result.data;
  }

  get totalAds(): number {
    return this.result.results;
  }

  get pageSize(): number {
    return this.result.pageSize;
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  handleDelete(adId: string): void {
    this.adService.deleteAdById(adId).subscribe((result) => {
      window.location.reload();
    });
  }

  goTo(aPage) {
    this.fetchUserAds(aPage);
  }
}
