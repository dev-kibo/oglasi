import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile-navigation',
  templateUrl: './profile-navigation.component.html',
  styleUrls: ['./profile-navigation.component.css'],
})
export class ProfileNavigationComponent implements AfterViewInit {
  constructor(private authService: AuthService, private router: Router) {}

  ngAfterViewInit(): void {
    const profileTab = document.querySelector('#v-pills-profile-tab');
    const adsTab = document.querySelector('#v-pills-ads-tab');

    const split = this.router.url.split('/');
    const currentRoute = split[split.length - 1];

    switch (currentRoute) {
      case 'my-ads':
        profileTab.classList.remove('active');
        adsTab.classList.add('active');
        break;
      case 'profile':
        adsTab.classList.remove('active');
        profileTab.classList.add('active');
        break;
      default:
        break;
    }
  }

  get isAdmin(): boolean {
    return this.authService.isAdmin();
  }
}
