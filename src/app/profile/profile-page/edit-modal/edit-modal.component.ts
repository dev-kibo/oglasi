import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import User from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

declare var $: any;

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css'],
})
export class EditModalComponent implements OnInit {
  @Input() user: User;
  @Output() updateUser = new EventEmitter<User>();

  editProfile;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.editProfile = new FormGroup({
      firstName: new FormControl(this.user.first_name, Validators.required),
      lastName: new FormControl(this.user.last_name, Validators.required),
      email: new FormControl(this.user.email, [
        Validators.required,
        Validators.email,
      ]),
      phone: new FormControl(this.user.phone, [
        Validators.required,
        Validators.pattern('\\d+'),
        Validators.minLength(9),
        Validators.maxLength(11),
      ]),
    });
  }

  onSubmit(event): void {
    event.preventDefault();

    console.dir(this.editProfile.value);

    this.userService
      .updateCurrentUser(this.editProfile.value)
      .subscribe((updatedUser) => {
        this.updateUser.emit(updatedUser);
        $('#exampleModal').modal('hide');
      });
  }
}
