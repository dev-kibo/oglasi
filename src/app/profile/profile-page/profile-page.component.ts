import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import User from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css'],
})
export class ProfilePageComponent implements OnInit {
  user: User;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.userService.getCurrentUserDetails().subscribe(
      (data) => {
        this.user = data;
      },
      (err) => {}
    );
  }

  get joinedAt(): string {
    return new Date(this.user.created_at).toLocaleDateString('sr', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    });
  }

  handleUpdatedUser(updatedUser): void {
    this.user = updatedUser;
  }
}
