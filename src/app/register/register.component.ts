import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  registerForm = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern('\\w+'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern('\\w+'),
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.pattern('[\\w]{6,}'),
    ]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [
      Validators.required,
      Validators.pattern('\\d+'),
      Validators.minLength(9),
      Validators.maxLength(11),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    confirmPassword: new FormControl('', [Validators.required]),
  });

  constructor(private authService: AuthService, private router: Router) {}

  onSubmit(event) {
    event.preventDefault();

    this.authService.register(this.registerForm.value).subscribe(
      () => {
        this.router.navigate(['login']);
      },
      (error) => {}
    );
  }
}
