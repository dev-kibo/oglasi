import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Ad from '../models/ad.model';
import Page from '../models/page.model';

@Injectable({
  providedIn: 'root',
})
export class AdService {
  constructor(private httpClient: HttpClient) {}

  createAd(ad, userId) {
    return this.httpClient
      .post(`${environment.apiCreateAd}`, {
        user_id: userId,
        city: ad['location'],
        street: ad['addr'],
        description: ad['desc'],
        vehicleCategory: ad['category'],
        vehicleType: ad['body'],
        make: ad['make'],
        model: ad['model'],
        travelled: ad['distance'],
        year_of_make: ad['modelYear'],
        fuelType: ad['fuel'],
        price: ad['price'],
        engine_capacity: ad['engineCC'],
        enginePower: ad['hp'],
        gearType: ad['transmission'],
        number_of_gears: ad['gears'],
        is_damaged: ad['damage'],
        powertrainType: ad['wheelDrive'],
        showcaseImage: ad['showcaseImage'],
        images: ad['images'],
      })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getAllCars(page: number = 0): Observable<Page<Ad>> {
    return this.httpClient.get<Page<Ad>>(
      `${environment.apiAllCars}?page=${page}&perPage=12`,
      {
        observe: 'body',
      }
    );
  }

  getCarsByType(type: string): Observable<Ad[]> {
    return this.httpClient
      .get<Ad[]>(`${environment.apiVehicleType}/${type}`, {
        observe: 'body',
      })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getAdById(id: string): Observable<Ad> {
    return this.httpClient
      .get<Ad>(`${environment.apiAdById}/${id}`, {
        observe: 'body',
      })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getUsersAds(
    userId: string,
    page: number = 0,
    perPage: number = 5
  ): Observable<Page<Ad>> {
    return this.httpClient
      .get<Page<Ad>>(
        `${environment.apiUserAds}/${userId}?page=${page}&perPage=${perPage}`,
        { observe: 'body' }
      )
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getFilteredAds(filter: FormGroup) {
    return this.httpClient
      .post<Ad[]>(`${environment.apiFilterAds}`, filter.value, {
        observe: 'body',
      })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  updateAd(adId, ad) {
    return this.httpClient.put(`${environment.apiUpdateAd}/${adId}`, {
      city: ad['location'],
      street: ad['addr'],
      description: ad['desc'],
      vehicleCategory: ad['category'],
      vehicleType: ad['body'],
      make: ad['make'],
      model: ad['model'],
      travelled: ad['distance'],
      year_of_make: ad['modelYear'],
      fuelType: ad['fuel'],
      price: ad['price'],
      engine_capacity: ad['engineCC'],
      enginePower: ad['hp'],
      gearType: ad['transmission'],
      number_of_gears: ad['gears'],
      is_damaged: ad['damage'],
      powertrainType: ad['wheelDrive'],
    });
  }

  deleteAdById(id: string) {
    return this.httpClient.delete(`${environment.apiDeleteAd}/${id}`);
  }
}
