import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import User from '../models/user.model';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  redirectUrl: string = '';

  constructor(private httpClient: HttpClient, private router: Router) {}

  isLoggedIn(): boolean {
    if (localStorage.getItem('access_token')) {
      return true;
    }
    return false;
  }

  isAdmin(): boolean {
    const token = localStorage.getItem('access_token');
    if (token === null) {
      return false;
    }

    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);

    if (decodedToken.role === 'admin') {
      return true;
    }

    return false;
  }

  login({ username, password }): Observable<Object> {
    return this.httpClient
      .post(
        environment.apiLogin,
        {
          username,
          password,
        },
        { observe: 'response' }
      )
      .pipe(
        tap((response) => {
          localStorage.setItem(
            'access_token',
            response.headers.get('access_token')
          );
        }),
        catchError((error) => {
          throw error;
        })
      );
  }

  register({
    firstName,
    lastName,
    username,
    email,
    phone,
    password,
  }): Observable<Object> {
    return this.httpClient
      .post(
        environment.apiRegister,
        {
          firstName,
          lastName,
          username,
          email,
          phone,
          password,
        },
        { observe: 'response' }
      )
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  logout(): void {
    localStorage.removeItem('access_token');
    this.router.navigate(['login']);
  }
}
