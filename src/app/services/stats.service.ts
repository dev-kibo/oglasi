import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Ad from '../models/ad.model';
import Dashboard from '../models/dashboard.model';
import Page from '../models/page.model';
import User from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class StatsService {
  constructor(private httpClient: HttpClient) {}

  getDashboardStats(): Observable<Dashboard> {
    return this.httpClient
      .get<Dashboard>(`${environment.apiStatsDashboard}`, { observe: 'body' })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getUserStats(aPage = 0, perPage = 5): Observable<Page<User>> {
    return this.httpClient
      .get<Page<User>>(
        `${environment.apiStatsUsers}?page=${aPage}&perPage=${perPage}`,
        { observe: 'body' }
      )
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getAdStats(page = 0, perPage = 5): Observable<Page<Ad>> {
    return this.httpClient
      .get<Page<Ad>>(
        `${environment.apiStatsAds}?page=${page}&perPage=${perPage}`,
        { observe: 'body' }
      )
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }
}
