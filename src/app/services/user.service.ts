import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import User from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  getCurrentUserDetails() {
    return this.httpClient
      .get<User>(environment.apiCurrentUser, { observe: 'body' })
      .pipe(
        catchError((error) => {
          throw error;
        })
      );
  }

  getUserById(id) {
    return this.httpClient.get<User>(`${environment.apiUserById}/${id}`, {
      observe: 'body',
    });
  }

  updateCurrentUser(updateInfo) {
    return this.getCurrentUserDetails().pipe(
      mergeMap((user) =>
        this.httpClient.put<User>(
          `${environment.apiUpdateUser}/${user.id}`,
          {
            first_name: updateInfo['firstName'],
            last_name: updateInfo['lastName'],
            email: updateInfo['email'],
            phone: updateInfo['phone'],
          },
          { observe: 'body' }
        )
      )
    );
  }

  deleteUserById(id: string) {
    return this.httpClient.delete(`${environment.apiDeleteUser}/${id}`);
  }
}
