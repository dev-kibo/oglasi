import {
  ApplicationRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { BIKE_BODIES } from 'src/app/values/bikes.value';
//jquery
declare var $: any;
@Component({
  selector: 'app-side-panel-bikes',
  templateUrl: './bikes.component.html',
  styleUrls: ['./bikes.component.css'],
})
export class BikesComponent implements OnInit {
  isHidden: boolean = true;
  @Output() vehicleType = new EventEmitter<string>();

  bikes: string[] = BIKE_BODIES;

  constructor(private applicationRef: ApplicationRef) {}

  ngOnInit(): void {
    $('#collapseTwo').on('show.bs.collapse', () => {
      this.isHidden = false;
      this.applicationRef.tick();
    });
    $('#collapseTwo').on('hide.bs.collapse', () => {
      this.isHidden = true;
      this.applicationRef.tick();
    });
  }

  onClick(value) {
    this.vehicleType.emit(value);
  }
}
