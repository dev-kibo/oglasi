import {
  ApplicationRef,
  Component,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { CAR_BODIES } from 'src/app/values/cars.value';
// jquery
declare var $: any;
@Component({
  selector: 'app-side-panel-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
})
export class CarsComponent implements OnInit {
  @Output() vehicleType = new EventEmitter<string>();

  cars: string[] = CAR_BODIES;

  isHidden: boolean = false;

  constructor(private applicationRef: ApplicationRef) {}

  ngOnInit(): void {
    $('#collapseOne').on('show.bs.collapse', () => {
      this.isHidden = false;
      this.applicationRef.tick();
    });
    $('#collapseOne').on('hide.bs.collapse', () => {
      this.isHidden = true;
      this.applicationRef.tick();
    });
  }

  onClick(value) {
    this.vehicleType.emit(value);
  }
}
