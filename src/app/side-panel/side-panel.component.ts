import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.css'],
})
export class SidePanelComponent {
  @Output() vehicleType = new EventEmitter<string>();

  constructor() {}

  handleClick(type) {
    this.vehicleType.emit(type);
  }
}
