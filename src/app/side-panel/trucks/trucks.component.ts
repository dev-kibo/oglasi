import {
  ApplicationRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { TRUCK_BODIES } from 'src/app/values/trucks.value';

//jquery
declare var $: any;
@Component({
  selector: 'app-side-panel-trucks',
  templateUrl: './trucks.component.html',
  styleUrls: ['./trucks.component.css'],
})
export class TrucksComponent implements OnInit {
  isHidden: boolean = true;
  @Output() vehicleType = new EventEmitter<string>();

  trucks: string[] = TRUCK_BODIES;

  constructor(private applicationRef: ApplicationRef) {}

  ngOnInit(): void {
    $('#collapseThree').on('show.bs.collapse', () => {
      this.isHidden = false;
      this.applicationRef.tick();
    });
    $('#collapseThree').on('hide.bs.collapse', () => {
      this.isHidden = true;
      this.applicationRef.tick();
    });
  }

  onClick(value) {
    this.vehicleType.emit(value);
  }
}
