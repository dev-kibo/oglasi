export const TRUCKS: string[] = [
  'DAF',
  'FAP',
  'Gaz',
  'Iveco',
  'Kamaz',
  'MAN',
  'Renault',
  'Peugeot',
  'Scania',
  'Volkswagen',
  'Volvo',
  'Isuzu',
  'Kenworth',
  'Peterbilt',
];

export const TRUCK_BODIES: string[] = [
  'Tractor unit',
  'Flatbed',
  'Dump',
  'Box',
];
