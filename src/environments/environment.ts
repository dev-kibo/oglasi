// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const baseApiUrl: string = 'http://localhost:8080/api';

export const environment = {
  production: false,
  apiLogin: 'http://localhost:8080/login',
  apiRegister: `${baseApiUrl}/users/register`,
  apiAllCars: `${baseApiUrl}/ads/all`,
  apiVehicleType: `${baseApiUrl}/ads/vehicle`,
  apiAdById: `${baseApiUrl}/ads`,
  apiCurrentUser: `${baseApiUrl}/users/me`,
  apiUserAds: `${baseApiUrl}/ads/user`,
  apiDeleteAd: `${baseApiUrl}/ads/delete`,
  apiStatsDashboard: `${baseApiUrl}/stats/dashboard`,
  apiStatsUsers: `${baseApiUrl}/stats/users`,
  apiStatsAds: `${baseApiUrl}/stats/ads`,
  apiFilterAds: `${baseApiUrl}/ads/filter`,
  apiUpdateUser: `${baseApiUrl}/users/update/data`,
  apiCreateAd: `${baseApiUrl}/ads/create`,
  apiUserById: `${baseApiUrl}/users`,
  apiDeleteUser: `${baseApiUrl}/users/delete`,
  apiUpdateAd: `${baseApiUrl}/ads/update`,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
